# encoding: utf-8

require_relative "msmlibs/lexer"
require_relative "msmlibs/syntaxtree"

$state =""
$stq = Array.new

def char_diff(base_seq, target)
  state =""
  pos = 0
  base_seq.each_char { |c|
    #puts "char_diff : #{c} vs #{target[pos]}"
    if c == target[pos]
      pos += 1
    else
      if c == "_"
        state += target[pos]
        pos += 1
      else
        return false # with no state update
      end
    end
  }
  if state.size > 0
    $state  += state
  end
  return true
end

def block_execute(l, string)
  l.each { |seq|
    puts " ]] #{seq}"
    if seq.class == Array
      if not block_execute(seq, string)
        $state ="!"
        return false
      else
        return true
      end
    else
      if char_diff(seq, string)
        string = string[seq.length..-1]
        $stq.push string
        return true
      else
        ;
      end
    end
  }
  return false
end

def execute(s, string)
  puts "Executing machine \"#{s}\" with #{string}"
  m = Moore::Syntaxtree.new(s)
  m.build_tree
  m.bigtree.each { |seq|
    puts " >> #{seq}"
    if seq.class == Array
      if not block_execute(seq, string)
        $state  = "!"
        break
      end
      string = $stq.last
    else
      if char_diff(seq, string)
        string = string[seq.length..-1]
      else
        $state = "!"
        break
      end
    end
  }
  if $state == ""
    $state = "_" # can not guess
  end
end


if __FILE__ == $0
  if ARGV.size < 2 
    puts "2014 ACM ICPC Asia-Daejeon Regional Internet Contest - Practice C"
    puts "Moore state machine question solution in Ruby"
    puts ""
    puts " Usage: #{__FILE__} (Machine sequence in string) (Target string)"
    puts "        #{__FILE__} \"AB(CD|FE|__)EE(A|Z(E|F))\" \"ABFEEEZF\""
    exit -1
  else
    execute(ARGV[0], ARGV[1])
    puts "RETURN STATE: (#{$state})"
  end
end
