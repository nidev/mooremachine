# encoding: utf-8

module Moore
  class Lexer
    def initialize(str)
      puts "lexer initialized"
      @string = str
      @pos = 0
    end

    def more?
      return @pos < (@string.size)
    end

    def reset
      @pos = 0
    end

    def peek
      return @string[@pos+1]
    end

    def next
      acc = Array.new
      while more? do
        case @string[@pos]
        when /[A-Z_]/
          acc.push @string[@pos]
          if peek.nil? or peek.match(/[\|\(\)]/)
            @pos += 1
            ret = acc.join
            acc.clear
            return ret
          end
          @pos += 1
        when /[\|\(\)]/e
          ret = @string[@pos]
          @pos += 1
          return ret
        end
      end

      if not acc.empty?
        ret = acc.join
        acc.clear
        return ret
      else
        return nil
      end
    end
  end
end
