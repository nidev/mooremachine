# encoding: utf-8
require_relative "lexer"

module Moore
  class Syntaxtree
    # :char, :block
    attr_accessor :bigtree
    def initialize(machine_str)
      puts "parsing tree initialized"
      @lexer = Lexer.new(machine_str)
      @bigtree = Array.new
    end

    def build_tree
      while true do
        l = @lexer.next
        if l.nil?
          break
        elsif l.match(/[A-Z_]+/)
          @bigtree.push l
        elsif l == "("
          @bigtree.push block()
        elsif l == ")"
          ;
        else
          puts "??????????"
        end
      end
      puts @bigtree.inspect
    end

    def block
      minitree = Array.new
      while true do
        l = @lexer.next
        if l.match(/[A-Z_]+/)
          minitree.push l
        elsif l == "("
          minitree.push block()
        elsif l == ")"
          #puts "finish"
          break
        end
      end
      return minitree
    end
  end
end

